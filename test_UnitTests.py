import unittest
import src.CalculatriceClassique as CalculatriceClassique

class TestCalculatriceClassique(unittest.TestCase):
    def test_addition(self):
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.addition(0,0), 0)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.addition(5,5), 10)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.addition(23,45), 68)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.addition(45,23), 68)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.addition(-5,8), 3)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.addition(8,-5), 3)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.addition(-5,-3), -8)
    def test_soustraction(self):
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.soustraction(0,0), 0)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.soustraction(5,5), 0)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.soustraction(23,45), -22)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.soustraction(45,23), 22)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.soustraction(-5,8), -13)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.soustraction(8,-5), 13)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.soustraction(-5,-3), -2)
    def test_multiplication(self):
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.multiplication(0,0), 0)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.multiplication(5,5), 25)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.multiplication(23,45), 1035)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.multiplication(45,23), 1035)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.multiplication(-5,8), -40)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.multiplication(8,-5), -40)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.multiplication(-5,-3), 15)
    def test_division(self):
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.division(23,0), None)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.division(0,23), 0)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.division(0,0), None)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.division(5,5), 1)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.division(23,46), 0.5)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.division(46,23), 2)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.division(23,460000), 0.00005)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.division(-5,8), -0.625)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.division(5,-8), -0.625)
        self.assertEqual(CalculatriceClassique.CalculatriceClassique.division(-10,-2), 5)
    def test_calc(self):
        # Contexte (Les classes sont déjà statiques)
        CalculatriceClassique.CalculatriceClassique.purgeHistoric()
        # Stimulis
        CalculatriceClassique.CalculatriceClassique.addition(4,5)
        CalculatriceClassique.CalculatriceClassique.soustraction(10,3)
        CalculatriceClassique.CalculatriceClassique.multiplication(3,5)
        CalculatriceClassique.CalculatriceClassique.division(10,2)
        # Tests
        reponse = "[('4+5', 9), ('10-3', 7), ('3*5', 15), ('10/2', 5.0)]"
        self.assertEqual(str(CalculatriceClassique.CalculatriceClassique.getHistoric()), reponse)

if __name__ == '__main__':
    unittest.main()