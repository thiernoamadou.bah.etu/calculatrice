import src.CalculatriceClassique as CalculatriceClassique

class CalculatriceAvancee(CalculatriceClassique.CalculatriceClassique):
    @staticmethod
    def puissance():
        raise NotImplementedError

    @staticmethod
    def cosinus():
        raise NotImplementedError

    @staticmethod
    def sinus():
        raise NotImplementedError

    @staticmethod
    def tangeante():
        raise NotImplementedError

    @staticmethod
    def logarithme():
        raise NotImplementedError

    @staticmethod
    def logarithmeNeperien():
        raise NotImplementedError