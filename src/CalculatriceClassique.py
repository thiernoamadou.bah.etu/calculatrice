class CalculatriceClassique:
    _historic = []

    @staticmethod
    def addition(val1, val2):
        CalculatriceClassique._historic.append((str(val1)+'+'+str(val2), val1+val2))
        return val1+val2

    @staticmethod
    def soustraction(val1, val2):
        CalculatriceClassique._historic.append((str(val1)+'-'+str(val2), val1-val2))
        return val1-val2

    @staticmethod
    def multiplication(val1, val2):
        CalculatriceClassique._historic.append((str(val1)+'*'+str(val2), val1*val2))
        return val1*val2

    @staticmethod
    def division(val1, val2):
        if val2 == 0:
            CalculatriceClassique._historic.append((str(val1)+'/'+str(val2), None))
            return None
        else:
            CalculatriceClassique._historic.append((str(val1)+'/'+str(val2), val1/val2))
            return val1/val2

    @staticmethod
    def purgeHistoric():
        CalculatriceClassique._historic.clear()

    @staticmethod
    def getHistoric():
        return CalculatriceClassique._historic